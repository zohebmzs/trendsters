<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>To_send_an_email_alert_to_the_end_user_when_the_order_is_delivered</fullName>
        <ccEmails>pvp0104@gmail.com</ccEmails>
        <description>To send an email alert to the end user when the order is delivered</description>
        <protected>false</protected>
        <recipients>
            <recipient>enduser@trendsters.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Information/Sales_Rep_Info</template>
    </alerts>
    <fieldUpdates>
        <fullName>OM_Update_to_discharged</fullName>
        <field>OM_Order_status__c</field>
        <literalValue>discharged</literalValue>
        <name>OM_Update to discharged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OM_update_to_discharged1</fullName>
        <field>OM_Order_status__c</field>
        <literalValue>discharged</literalValue>
        <name>OM_update to discharged1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>OM_Update to discharged</fullName>
        <active>true</active>
        <criteriaItems>
            <field>OM_Order__c.OM_Order_status__c</field>
            <operation>equals</operation>
            <value>placed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>To_send_an_email_alert_to_the_end_user_when_the_order_is_delivered</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>OM_update_to_discharged1</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
