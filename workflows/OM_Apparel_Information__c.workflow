<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Apparel_Info</fullName>
        <ccEmails>zohebmzsedu@gmail.com</ccEmails>
        <description>Apparel Info</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Representative</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Information/Sales_Rep_Info</template>
    </alerts>
    <fieldUpdates>
        <fullName>UpdateStockAvb</fullName>
        <description>Update stock available to out of stock if no of pieces = 0</description>
        <field>OM_Stock_Avialability__c</field>
        <literalValue>Out of Stock</literalValue>
        <name>UpdateStockAvb</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>in_stock_update</fullName>
        <description>to update the stock availability to in stock if number of pieces is not equal to 0</description>
        <field>OM_Stock_Avialability__c</field>
        <literalValue>In Stock</literalValue>
        <name>In stock update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_stock_till</fullName>
        <description>update the stock till to today&apos;s date if the number of pieces is equal to 0 today</description>
        <field>OM_In_Stock_till__c</field>
        <formula>today()</formula>
        <name>update stock till</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_stock_till1</fullName>
        <description>stock till date has to be equal to today plus 4 if the number of pieces is greater than 20</description>
        <field>OM_In_Stock_till__c</field>
        <formula>today()+4</formula>
        <name>update stock till1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_stock_till2</fullName>
        <description>stock till date has to equal to today plus 1 if the number of pieces is less than 20</description>
        <field>OM_In_Stock_till__c</field>
        <formula>today()+1</formula>
        <name>update stock till2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ChangeAvb</fullName>
        <actions>
            <name>UpdateStockAvb</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_stock_till</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OM_Apparel_Information__c.OM_Available_Pieces__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>Change Avaliablity to out of stock and stock last availability date to today if no of pieces = 0</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send email with apparelinfo</fullName>
        <active>false</active>
        <criteriaItems>
            <field>OM_Apparel_Information__c.OM_Available_Pieces__c</field>
            <operation>lessOrEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Send Email to sales rep</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>update number of pieces</fullName>
        <actions>
            <name>in_stock_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OM_Apparel_Information__c.OM_Available_Pieces__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>if number of pieces &gt;0 then the stock availability is set to in stock</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update stock till1</fullName>
        <actions>
            <name>update_stock_till1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OM_Apparel_Information__c.OM_Available_Pieces__c</field>
            <operation>greaterThan</operation>
            <value>20</value>
        </criteriaItems>
        <description>update the stock availability date to +4days if no of pieces &gt;20</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update stock till2</fullName>
        <actions>
            <name>update_stock_till2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OM_Apparel_Information__c.OM_Available_Pieces__c</field>
            <operation>lessThan</operation>
            <value>20</value>
        </criteriaItems>
        <description>Update the stock availability date to today +1 if the no of pieces&lt;20</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
