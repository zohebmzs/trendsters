public class Items {
    
    private final OM_Apparel_Information__c app;
    public string colorPickvalue{get; set;}
    public string typePickvalue{get; set;}
    public string genderPickvalue{get; set;}
    public string name{get; set;}
    public string currentItemPrice{get;set;}
    public Id currentRecordId{get;set;}
    Public string accid{get;set;}
    Public List<Account> revenue{get;set;}
    Public List<Account> revenue1{get;set;}
    
    public list<OM_Apparel_Information__c> itemCount{get;set;}
    //integer count;
    //private final Task myTask;
    public List<OM_Apparel_Information__c> AppList {get; set;}
    public ApexPages.StandardSetController stdCntrlr {get; set;}
    public List<OM_Apparel_Information__c> accList {get; set;}
    public List<OM_Order__c> orders{get; set;}
    public List<OM_Order__c> status{get; set;}
    public List<OM_Order__c> status1{get; set;}
    public List<Account> accounts{get;set;}
    public List<Account> emailAlert{get;set;}
    public List<OM_Order__c> OrderId{get; set;}
    public String searchText;
    public Static Integer x1=0;
    public list<OM_Order__c> results;
    public list<OM_Apparel_Information__c> NoOfItems;
    public List<OM_Apparel_Information__c> price;
    public List<String> apparrelIds{get; set;}
    public List<OM_Apparel_Information__c> accs{get; set;}
    public Integer x{get;set;}
    Public String char1;
    public String[] pricee{get;set;}
    public Items(ApexPages.StandardController stdController) {
        accs = new List<OM_Apparel_Information__c>();
        price = new list<OM_Apparel_Information__c>();
        AppList = new List<OM_Apparel_Information__c>();
        apparrelIds = new List<String>();
        accounts = new List<Account>();
        accid = '';
        AppList = [Select Id, Name, OM_price__c, OM_Apparel_Images__c,OM_Age_Group__c,OM_Color__c,OM_Gender__c,OM_Available_Pieces__c From OM_Apparel_Information__c];
        this.app = (OM_Apparel_Information__c)stdController.getRecord();        
    }
    
    public Items(ApexPages.StandardSetController controller) {
        stdCntrlr = controller;
    }
    
    public void getaccounts() {
        accList = new list<OM_Apparel_Information__c>([SELECT Id, OM_price__c, OM_Apparel_Images__c,OM_Age_Group__c,OM_Color__c,OM_Gender__c,OM_Apparel_Types__c from OM_Apparel_Information__c WHERE OM_Color__c =: colorPickvalue or OM_Gender__c =: genderPickvalue or OM_Apparel_Types__c =: typePickvalue]);
    }  
    public string function(){
        return name;
    }

    public String getSearchText() {
        return searchText;
    }
    
    public void setSearchText(String s) {
        searchText = s;
    }
    
    public List<OM_Order__c> getResults() {
        return results;
    }

    public PageReference doSearch() {
        results = new list<OM_Order__c>([select Id, Total__c from OM_Order__c where Name=:name limit 1]);
        return null;
    }
    public void addToCart(){
        pricee = currentItemPrice.split(',');
        itemCount = new List<OM_Apparel_Information__c>([select OM_Available_Pieces__c from OM_Apparel_Information__c where Id=:pricee[1]]);
        if(itemCount[0].OM_Available_Pieces__c == 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error Message');
            ApexPages.addMessage(myMsg);
        }
        else{
            itemCount[0].OM_Available_Pieces__c--;
            update itemCount[0];
        }
        this.char1 = 'h';
        results[0].Total__c += integer.valueof(pricee[0]);
        //query and reduce
        status = new List<OM_Order__c>([select OM_Order_status__c from OM_Order__c where Id=:results[0].Id]);
        status[0].OM_Order_status__c ='placed_in_cart';
        update status[0];
        update results[0];
    }
    
    public void MakePayment(){
        //itemCount = new List<OM_Apparel_Information__c>([select OM_Available_Pieces__c from OM_Apparel_Information__c where Id=:item1[1].substring(0,14)]);
        revenue = new List<Account>([select AnnualRevenue from Account where Name=:'SalesRep']);
        System.debug(name);
        emailAlert = new List<Account>([select OM_email__c from Account where Name=: name]);
        Integer x = integer.valueof(results[0].Total__c);
        status1 = new List<OM_Order__c>([select OM_Order_status__c from OM_Order__c where Id=:results[0].Id]);
        revenue1 = new List<Account>([select OM_NoOfOrders__c, AnnualRevenue from Account where Name=:name]);
        revenue1[0].OM_NoOfOrders__c++;
        revenue1[0].AnnualRevenue+=x;
        status1[0].OM_Order_status__c = 'drafted';
        revenue[0].AnnualRevenue += x;
        results[0].Total__c = 0;
        update status1[0];
        update revenue[0];
        update revenue1[0];
        update results[0];        
        //sending email alert
        /*Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
message.toAddresses = new String[] {emailAlert[0].OM_email__c};
message.optOutPolicy = 'FILTER';
message.subject = 'Order placed';
message.plainTextBody = 'Your order has been placed';
Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
Messaging.SendEmailResult[] results1 = Messaging.sendEmail(messages);

if (results1[0].success) 
{
System.debug('The email was sent successfully.');
} else 
{
System.debug('The email failed to send: ' + results1[0].errors[0].message);
}
*/
        //NoOfItems[0].OM_Available_Pieces__c--;
        
        //update NoOfItems[0];
    }
}