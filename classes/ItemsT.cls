@istest(SeeAlldata=True)
private class ItemsT {

     private static testmethod void TestMethod1(){
         /**** Set page in context ****/
        Test.setCurrentPage(Page.OM_Addtocart1);    
        /**** Set Controller in context ****/
        Items controllerExt = new Items(new ApexPages.StandardController(new OM_Apparel_Information__c()));
        
         OM_Apparel_Information__c oApparelinfo = new OM_Apparel_Information__c();
         oApparelinfo.Name= 'Test';
         oApparelinfo.OM_Age_Group__c ='25-27';
         oApparelinfo.OM_Apparel_Types__c ='shirts';
         oApparelinfo.OM_price__c =500;
         oApparelinfo.OM_Color__c='blue';
         oApparelinfo.OM_Brand_Name__c ='Adidas';
         oApparelinfo.OM_Designer__c ='Julkaker Singh';
         oApparelinfo.OM_Stock_Avialability__c ='Out of Stock';
         oApparelinfo.OM_Available_Pieces__c=5;
         oApparelinfo.OM_In_Stock_till__c=Date.today(); 
         oApparelinfo.OM_Introduced_On__c=Date.today()-1;
         oApparelinfo.OM_Gender__c='Female';
         insert oApparelinfo;
         
         Account acc = new Account();
         acc.Name='SalesRep';
         acc.AnnualRevenue=2000;
         
                  
         controllerExt.getaccounts();
         controllerExt.getResults();
         controllerExt.MakePayment();
         controllerExt.addToCart();
    }

}